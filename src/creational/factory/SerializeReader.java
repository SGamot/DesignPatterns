package creational.factory;

public class SerializeReader implements DataAccesor
{
    public void readData()
    {
        System.out.println("Reading from SerializedObject");
    }
}
