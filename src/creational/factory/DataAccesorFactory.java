package creational.factory;


public class DataAccesorFactory
{
    public static DataAccesor getInstance(DataPoints type)
    {
        switch(type)
        {
            case JSON : return new JsonReader();
            case XML : return new XMLReader();
            case DATABASE : return new DatabaseReader();
            case SERIALIZED : return new SerializeReader();
        }
        
        return null;
    }
    
}
