package creational.factory;

public class XMLReader implements DataAccesor
{
    public void readData()
    {
        System.out.println("Reading from XML");
    }
}
