package creational.factory;

public class FactoryTest
{
    public static void main(String args[])
    {
        DataAccesorFactory factory = new DataAccesorFactory();
        DataAccesor reader =  factory.getInstance(DataPoints.DATABASE);
        reader.readData();
        reader = factory.getInstance(DataPoints.XML);
        reader.readData();
        
    }
}
