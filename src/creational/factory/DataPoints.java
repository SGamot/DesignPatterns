package creational.factory;

public enum DataPoints
{
    JSON,
    XML,
    DATABASE,
    SERIALIZED,
    FILE
}
