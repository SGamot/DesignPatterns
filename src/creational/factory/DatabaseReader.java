package creational.factory;

public class DatabaseReader implements DataAccesor
{
    public void readData()
    {
        System.out.println("Reading from Database");
    }
}
