package creational.factory;


public class JsonReader implements DataAccesor
{
    public void readData()
    {
        System.out.println("Reading from JSON");
    }
}
