package creational.builder;

public class Employee
{
  private int    id;
  private String firstName;
  private String lastName;
  private String department;
  private String role;
  private int    salary;
  
  public Employee(EmployeeBuilder b)
  {
    this.firstName = b.getFirstName();
    this.lastName = b.getLastName();
    this.role = b.getRole();
    this.department = b.getDepartment();
    this.salary = b.getSalary();
    this.id= b.getId();
  }
  
  public String getFirstName()
  {
    return firstName;
  }
  
  public String getLastName()
  {
    return lastName;
  }
  
  public String getRole()
  {
    return role;
  }
  
  public String getDepartment()
  {
    return department;
  }
  
  public int getSalary()
  {
    return salary;
  }
  
  public int getId()
  {
    return id;
  }
  
  @Override public String toString()
  {
    return "Employee{" + "id=" + id + ", firstName='" + firstName + '\''
            + ", lastName='" + lastName + '\'' + ", department='" + department
            + '\'' + ", role='" + role + '\'' + ", salary=" + salary + '}';
  }
}
