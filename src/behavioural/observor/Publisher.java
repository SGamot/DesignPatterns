package behavioural.observor;

import java.util.ArrayList;

public class Publisher
{
  ArrayList<Subscriber> listOfSubs = new ArrayList();
  String data = "";
  String name = "";
  
  public Publisher(String name){
    this.name = name;
  }
  public Publisher(String name, String data)
  {
    this(name);
    this.data=data;
  }
  
  public String getName(){
    return this.name;
  }
  
  public void setName(String name){
    this.name = name;
  }
  
  public void addSub(Subscriber s)
  {
    listOfSubs.add(s);
  }
  
  public void updateData(String data)
  {
    this.data = data;
    notifySubs();
  }
  
  public void notifySubs(){
    listOfSubs.forEach(subscriber -> subscriber.update(this.name,this.data));
  }
}
