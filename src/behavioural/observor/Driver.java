package behavioural.observor;

public class Driver
{
  public static void main(String[] args)
  {
    Publisher sed_lyf_channel = new Publisher("Sed_Lyf_Nishant");
    Publisher no_skills_nishant_channel = new Publisher("No Skills Nishant");
    
    Subscriber sub1 = new Subscriber();
    Subscriber sub2 = new Subscriber();
    Subscriber sub3 = new Subscriber();
    Subscriber sub4 = new Subscriber();
    
    sub1.subscribe(sed_lyf_channel);
    sub2.subscribe(sed_lyf_channel);
    sub2.subscribe(no_skills_nishant_channel);
    sub4.subscribe(no_skills_nishant_channel);
    
    sed_lyf_channel.updateData("Nishant has to wash his dishes");
    no_skills_nishant_channel.updateData("Importing yet another library");
  }
}
