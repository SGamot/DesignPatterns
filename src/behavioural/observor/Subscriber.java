package behavioural.observor;

import java.util.ArrayList;

public class Subscriber
{
  ArrayList<Publisher> publisherList = new ArrayList();
  
  public void subscribe(Publisher o)
  {
      publisherList.add(o);
      o.addSub(this);
  }
  
  public void update(String name , String data){
    System.out.printf("Message: \"%s\" from \"%s\" @ Subscriber: \"%s\" \n", data,name,this.toString());
  }
}
